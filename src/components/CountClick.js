import { useEffect, useState } from "react";

const CountClick = () => {
    const [count, setCount] = useState(0);

    const buttonClickHandler =() => {
        setCount(count + 1);
    }

    useEffect(() => {
        document.title = `You clicked ${count} times`
    }, [count]);

    return(
        <div>
        <p>You clicked {count} times</p>
        <br />
        <button onClick={buttonClickHandler}>Click Me</button>
        </div>
    )
}

export default CountClick;